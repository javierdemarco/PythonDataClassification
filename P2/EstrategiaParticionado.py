# -*- coding: utf-8 -*-
from abc import ABCMeta,abstractmethod
import numpy as np
import pandas as pd


class Particion():
  """
  Clase particion que mantiene los indices que van a componer una particion.
  Estos indices son filas en la numpy array de datos
  """
  # Atributos
  indicesTrain = list()
  indicesTest = list()


  def __init__(self, test, train):
    self.indicesTrain = train
    self.indicesTest = test
    return

####################################################################################################

class EstrategiaParticionado(object):
  """
  Clase que define la estrategia de particionado.
  """
  # Clase abstracta
  __metaclass__ = ABCMeta

  #Atributos
  nombreEstrategia = ""
  numeroParticiones = 0
  listaParticiones = list()
  datos = np.array(())

  #Constructor
  def __init__(self, datos, nombre, numero, lista):
    self.datos = datos
    self.nombreEstrategia = nombre
    self.numeroParticiones = numero
    self.listaParticiones = lista
    return

  @abstractmethod
  def creaParticiones(self, datos, seed=None):
    pass


####################################################################################################

class ValidacionSimple(EstrategiaParticionado):
  """
  Crea particiones segun el metodo tradicional de division de los datos segun el
  porcentaje deseado.
  Devuelve una lista de particiones de tipo clase Particion.
  """
  #Atributos
  porcentaje = 0

  #Constructor
  def __init__(self, datos, nombre, numero, lista, porcentaje):
    super().__init__(datos, nombre, numero, lista)
    self.porcentaje = porcentaje
    self.creaParticiones(datos, pd.datetime.now().microsecond)
    return

  #Funcion de construccion de paticiones
  def creaParticiones(self,datos, seed=None,):
    #Atributos locales
    train = list()
    test = list()
    tamDatos = int(len(datos))
    indices = list(range(tamDatos))

    #Semilla inicializacion
    np.random.seed(seed)
    #Añade indices hasta el tamaño del porcentaje
    train = sorted(list(np.random.choice(tamDatos - 1, size= int(tamDatos * self.porcentaje)
                          , replace=False)))
    #Agrega el restante de indices a test
    test = [x for x in indices if x not in train]
    #Crea las clase partciones
    self.listaParticiones.append(Particion(train,test))
    return


#####################################################################################################
class ValidacionCruzada(EstrategiaParticionado):
  """
  Crea particiones segun el metodo de validacion cruzada.
  El conjunto de entrenamiento se crea con las nfolds-1 particiones y el de test con la particion
  restante.
  Devuelve una lista de particiones de tipo Particion
  """

  #Constructor
  def __init__(self, datos, nombre, numero, lista):
    super().__init__(datos, nombre, numero, lista)
    self.creaParticiones(datos, pd.datetime.now().microsecond)
    return

  #Funcion de construccion de paticiones
  def creaParticiones(self,datos, seed=None,):
    #Atributos locales
    conjuntoFolds = list(list())
    test = list()
    tamDatos = int(len(datos))
    indices = np.arange(tamDatos)
    tamFolds = int(tamDatos / self.numeroParticiones)
    tamRestantes = int(tamDatos % self.numeroParticiones)

    #Combio aleatorio de indices y convertido a lista para su uso posterior
    np.random.shuffle(indices)
    indices = list(indices)
    #Si sobra alguno porque no es division entera, se quitan de indices y se colocan en una lista
    #llada restantes para su posterior agregacion
    if (tamRestantes != 0):
      restantes = indices[-tamRestantes:]
      indices = indices[:-tamRestantes]
      #Se crean los conjuntos de tamaños especificados, quedando un ultimo grupo con los restantes
      conjuntoFolds = [indices[x:x+tamFolds] for x in range(0, len(indices), tamFolds)]
      #Estos se agregan empezando por el primer grupo
      [conjuntoFolds[i].append(restantes[i]) for i in range(len(restantes))]
    else:
      #Se crean los conjuntos de tamaños especificados, quedando un ultimo grupo con los restantes
      conjuntoFolds = [indices[x:x+tamFolds] for x in range(0, len(indices), tamFolds)]
    #Se crean todas las particiones iterando sobre el conjunto y asignando a test el resto
    for i in range(self.numeroParticiones):
      test = [x for x in conjuntoFolds if x != conjuntoFolds[i]]
      test = [x for y in test for x in y]
      self.listaParticiones.append(Particion(conjuntoFolds[i], test))
    return


#####################################################################################################
class ValidacionBootstrap(EstrategiaParticionado):
  """
  Crea particiones segun el metodo bootstrap con division de los datos segun el
  porcentaje deseado.
  Devuelve una lista de particiones de tipo clase Particion.
  """
  #Atributos
  porcentaje = 0

  #Constructor
  def __init__(self, datos, nombre, numero, lista, porcentaje):
    super().__init__(datos, nombre, numero, lista)
    self.porcentaje = porcentaje
    self.creaParticiones(datos, pd.datetime.now().microsecond)
    return

  #Funcion de construccion de paticiones
  def creaParticiones(self,datos, seed=None,):
    #Atributos locales
    train = list()
    test = list()
    tamDatos = int(len(datos))
    indices = list(range(tamDatos))
    
    i = 0
    while i < self.numeroParticiones:
      seed = seed + i
      #Semilla inicializacion
      np.random.seed(seed)
      #Añade indices hasta el tamaño del porcentaje
      train = sorted(list(np.random.choice(tamDatos - 1, size= int(tamDatos * self.porcentaje)
                            , replace=True)))
      #Agrega el restante de indices a test
      test = [x for x in indices if x not in train]
      #Crea las clase partciones
      self.listaParticiones.append(Particion(train,test))
      i += 1
    return



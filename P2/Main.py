# -*- coding: utf-8 -*-
from Datos import Datos
import EstrategiaParticionado as ep
import Clasificador as cl
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
import plotModel as pM

dataset=Datos('ConjuntosDatos_2/example1.data')

#################################VALIDACION SIMPLE#######################################
# strategy_simple = ep.ValidacionSimple(dataset.datos, "Validacion Simple", 1, list(), 0.5)

#################################VALIDACION CRUZADA#######################################
strategy_cruzada = ep.ValidacionCruzada(dataset.datos, "Validacion Cruzada", 5, list())

# #################################VALIDACION BOOTSTRAP#######################################
# strategy_bootstrap = ep.ValidacionBootstrap(dataset.datos, "Validacion Bootstrap", 5, list(), 0.6)

k = [1,3,5,11,21,51]
clasificador=cl.ClasificadorVecinosProximos("Vecinos Proximos", np.array(()), 
np.array(()), list(), dict(), True, 21)
errores_simple=clasificador.validacion(strategy_cruzada,dataset,clasificador)

# clasificador=cl.ClasificadorRegresionLogistica("Regresion Logistica", np.array(()), np.array(()), list(), dict(),1,50,[],True)
# errores_simple=clasificador.validacion(strategy_cruzada,dataset,clasificador)
# print("Error:",errores_simple)

# from plotModel import plotModel
# import matplotlib.pyplot as plt

# ii=strategy_cruzada.listaParticiones[-1].indicesTrain
# plotModel(dataset.datos[ii,0],dataset.datos[ii,1],dataset.datos[ii,-1]!=0,clasificador,"Frontera",dataset.diccionarios) 

# plt.figure()
# plt.plot(dataset.datos[dataset.datos[:,-1]==0,0], dataset.datos[dataset.datos[:,-1]==0,1],'bo')
# plt.plot(dataset.datos[dataset.datos[:,-1]==1,0], dataset.datos[dataset.datos[:,-1]==1,1],'ro')
# plt.show()



#SCICKIT LEARN

# X = []
# y = []
# for i in dataset.datos:
#   X.append(i[:-1])
#   y.append(i[-1])
# np.array(X)
# np.array(y)


# print("\nLogisticRegression")
# clf = LogisticRegression().fit(X, y)
# print("Score acierto ->",clf.score(X,y)*100,"%")

# print("\nKNeighborsClassifier-Uniforme")
# for k in [1,3,5,11,21,25]:
#   neigh = KNeighborsClassifier(n_neighbors=k,weights='uniform').fit(X, y) 
#   print("Score acierto con k=",k,"->",neigh.score(X,y)*100,"%")

# print("\nKNeighborsClassifier-Distancia")
# for k in [1,3,4,5,11,21,25]:
#   neigh = KNeighborsClassifier(n_neighbors=k,weights='distance').fit(X, y) 
#   print("Score acierto con k=",k,"->",neigh.score(X,y)*100,"%")
  

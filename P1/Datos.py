# -*- coding: utf-8 -*-
import numpy as np


class Datos():
    """
    Clase Datos, analiza un fichero con una estructura fija, dicho fichero 
    contiene:
        - Numero de Datos
        - Nombre de cada atributo
        - Tipo de cada atributo
        - Resto de fichero, datos a clasificar

    Se utiliza rstrip para omitir el retorno de carro de cada linea
    """
    #Continuo = NUMERO, Nominal = PALABRA
    TiposDeAtributos = ('Continuo', 'Nominal')
    datos = np.array(())
    nominalAtributos = []
    nombreAtributos=[]
    diccionarios = []
    tipoAtributos=[]
    numCol = 0
    numValores = 0
    nombreFichero = ""

    # Funcion inicializadora del objeto Datos
    def __init__(self, nombreFichero):
        with open(nombreFichero, "r") as f:
            i = 0
            self.nombreFichero = nombreFichero
            # Lee el fichero linea por linea
            for line in f:
                if i == 0:  # Primera linea Numero de valores
                    self.numValores = line
                elif i == 1:  # Segunda linea: Nombre de cada columna del atributo
                    self.nombreAtributos = line.rstrip("\n").rstrip("\r").split(",")
                    self.datos = np.empty((0, len(self.nombreAtributos)))
                # Tercera linea: El tipo de cada columna  que es un atributo
                elif i == 2: 
                    self.tipoAtributos = line.rstrip("\n").rstrip("\r").split(",")
                    self.numCol = len(self.tipoAtributos)
                    # Bucle que recorre el tipo de atributos leido para
                    # confirmar que el tipo indicado en el fichero es uno
                    # de los dos posibles registrados, Nominal o Continuo
                    for tipo in self.tipoAtributos:
                        # Comprobacion de tipo: TipoDeAtr[0]=Continuo 
                        # TipoDeAtr[1]=Nominal
                        if(tipo != self.TiposDeAtributos[0] and tipo 
                            != self.TiposDeAtributos[1]):
                            raise ValueError('Tipo de dato no registrado')
                        else:
                            # Crea un conjunto, los posibles valores de cada 
                            # atributo
                            self.diccionarios.append(set())
                    # Establece como True o False aquellos que sean Nominal
                    # o Continuo respectivamente

                    for each in line.rstrip("\n").rstrip("\r").split(","):
                        if (each == "Nominal"):
                            self.nominalAtributos.append(True)
                        elif (each == "Continuo"):
                            self.nominalAtributos.append(False)

                    # self.nominalAtributos = line.rstrip("\n").rstrip("\r").replace(
                    #     self.TiposDeAtributos[1], True).replace(
                    #     self.TiposDeAtributos[0], "False").split(",")
                else:  # Las demas lineas del fichero, Los datos propiamente dichos
                    k = 0
                    # Bucle que lee linea por linea y clasifica segun
                    # los datos leidos, introduciendolos en el set
                    for j in line.rstrip("\n").rstrip("\r").split(","):
                        if(self.nominalAtributos[k]==True):
                            self.diccionarios[k].add(j)
                        k += 1
                    # Introduce en la matriz datos, los datos leidos,
                    # separados por una "," y omitiendo el retorno de carro
                    self.datos = np.vstack(
                        [self.datos, line.rstrip("\n").rstrip("\r").split(",")])
                i += 1
        f.close()
        
        # Bucle creador de diccionario por cada atributo, con sus respectivos 
        # clave, valor
        i = 0
        for sortAux in self.diccionarios:
            self.diccionarios[i] = dict(enumerate(sorted(sortAux)))
            self.diccionarios[i] = {y: x for x,
                                    y in self.diccionarios[i].items()}
            i += 1

        # Bucle reemplazador de valores/atributos, por su repectivo
        # valor del diccionario anteriormente creado
        for aux in self.diccionarios:
            for valor, clave in enumerate(aux, 0):
                coord = np.transpose(np.where(self.datos == clave))
                for xCoord, yCoord in coord:
                    self.datos[xCoord][yCoord] = valor
                        
        self.datos=self.datos.astype(np.float)
        
    def extraeDatos(self, idx):
        datos_extraidos = list()
        for each in idx:
            datos_extraidos.append(self.datos[each])
        return datos_extraidos


import random
import numpy as np
import time
from abc import ABCMeta,abstractmethod


class Particion():
    """
    Clase que mantiene las listas de indices para las particiones
    de Train y Test.
    """
    indicesTrain = []
    indicesTest = []
    
    def __init__(self, indicesTrain, indicesTest):
        self.indicesTrain = indicesTrain
        self.indicesTest = indicesTest        
            
        
###############################################################################
class EstrategiaParticionado(object):
    """
    Clase abstracta para definir una estrategia de particionado
    Se especifica el nombre, el numero de particiones a realizar
    y la estructura de almacenaje de las mismas.
    Para clases que heredan, sera obligatorio definir la funcion
    crearParticiones, la cual se encargara de definir propiamente dicha
    la estrategia de particionado.    
    """
    #Atributos de la clase
    nombreEstrategia = ""
    numeroParticiones = 0
    particiones = []
    tam = 0

    # Clase abstracta
    __metaclass__ = ABCMeta
    
    def __init__(self, nombreEstrategia, numeroParticiones, particiones):
        self.nombreEstrategia = nombreEstrategia
        self.numeroParticiones = numeroParticiones
        self.particiones = particiones

    @abstractmethod
    def crearParticiones(self, datos):
        pass
    
    pass
        

###############################################################################
class ValidacionSimple(EstrategiaParticionado):
    """
    Utiliza el metodo tradicional de division de los datos.
    Esto sera definir un tamaño, tipicamente 50%, por el cual se
    obtendran las listas de entrenamiento y test.
    Estas listas estaran formadas por indices de los datos que se
    seleccionaran de manera aleatoria.
    """
    
    def __init__(self, porcentaje, datos):
        EstrategiaParticionado.__init__(self, "ValidacionSimple", 
            0, list())
        self.tam = porcentaje
        self.crearParticiones(datos)                       
    
    def crearParticiones(self, datos):
        numDatos = int(len(datos))
        train = set()
        test = set()
        indices = list(range(numDatos))
        """
        Se crea un generador de numeros aleatorios con la semilla del tiempo
        de ahora para que sea diferente cada vez que se ejecute.
        Este generador es el que despues se invocara para obtener aleatoriamente
        los indices de las particiones
        """
        
        """
        Se crea la particion train a partir de un bucle en el que
        iterando el tamaño obtenido por entrada, escogemos al azar los indices.
        A continuacion con una lista de comprension se crea la lista test
        como los indices que no estan en train pero si en indices
        completando asi las dos listas.
        """
        while len(train) < (numDatos * self.tam):
            train.add(np.random.randint(0, numDatos - 1))
        train = list(train)
        test = [x for x in indices if x not in train]
        # print(train)
        # print(test)
        #Una vez creadas las divisiones se añade la particion a la lista
        self.particiones.append(Particion(train,test))
        

###############################################################################
class ValidacionCruzada(EstrategiaParticionado):
    """
    Se utilza la estrategia de validacion cruzada que consiste
    en dividir los datos en grupos llamados folds, y de estos escoger
    secuencialmente que uno sea el test y los demas las particiones de train
    con lo que se crean tantas particiones como grupos haya.
    Asi por ejemplo si quiero 3 particiones, divido los datos en 3 grupos
    digamos 1-2-3 y creo las particiones
    Particion 1:
        Test: 1
        Train: 2-3
    Particion 2:
        Test: 2
        Train: 1-3
    Particion 3:
        Test: 3
        Train: 1-2
    """
    
    def __init__(self, numeroParticiones, datos):
        EstrategiaParticionado.__init__(self, "ValidacionCruzada", 
            numeroParticiones, list())
        self.crearParticiones(datos)       
    
    def crearParticiones(self, datos):
        # Numero de datos
        numDatos = int(len(datos))
        # Grupos de datos agrupados en numpy para su manipulacion
        conjuntosNumpy = np.array(())
        # Grupo de datos agrupados en listas de listas
        conjuntos = list()
        # Todos los indices de los datos de 0 a X
        indices = list(range(numDatos))
        # Se castea a array numpy para su manipulacion
        indices = np.array(indices)
        # Se cambia aleatoriamente los indices para su eleccion en grupos
        np.random.shuffle(indices)
        """
        # Esta variable nos dira cuantos numero son los que impiden que numpy
        # haga una division equitativa de los datos por lo que los quitamos
        # de ahi lastNumpy que es el array de datos ultimos que "sobran" 
        quitandolos de indices y asi asegurarnos que numpy hara una division
        equitativa
        """
        delete_numbers = (numDatos % self.numeroParticiones)
        lastNumpy = indices[-delete_numbers:]
        indices = np.setdiff1d(indices, lastNumpy, True)
        # Se realiza la division de datos
        conjuntosNumpy = np.split(indices, self.numeroParticiones, 0)
        # Se añaden los datos sobrantes a los diferentes grupos
        # Y se crea el conjuntos de listas de listas
        for each in conjuntosNumpy:
            conjuntos.append(list(each))
        for enum, each in enumerate(lastNumpy):
            conjuntos[enum].append(each)
        """
        Bucles de creacion de las particiones train y test.
        Como se ha explicado al principio, se selecciona secuencialmente
        la particion test y las demas seran las train, creando asi
        las diferentes particiones
        """
        for enum, each in enumerate(conjuntos):
            test = conjuntos[enum]
            train = []
            listaAux = list(conjuntos[:enum])
            listaAux.append(list(conjuntos[enum+1:]))
            for each in conjuntos[:enum]:
                for cada in each:
                    train.append(cada)  
            for each in conjuntos[enum+1:]:
                for cada in each:
                    train.append(cada)
            # print(train)
            # print(test)    
            self.particiones.append(Particion(train, test))


###############################################################################
class ValidacionBootstrap(EstrategiaParticionado):
  """
    Crea particiones segun el metodo de validacion por boostrap.
    Consiste en escojer aleatoriamente de entre los datos,
    hasta que el conjunto cumple que es del tamaño suministrado
    como entrada. Los restantes se vuleven el conjunto de test
  """

  def __init__(self, numeroParticiones, datos, tam):
        EstrategiaParticionado.__init__(self, "ValidacionBootstrap", 
            numeroParticiones, list())
        self.tam = tam
        self.crearParticiones(datos)       

  def crearParticiones(self,datos):   
    numDatos = int(len(datos))
    train = set()
    test = set()
    indices = list(range(numDatos))
    """
    Creo un generador de numero aleatorios con la semilla del tiempo
    de ahora para que sea diferente cada vez que se ejecute.
    Este generador es el que despues se invocara para obtener aleatoriamente
    los indices de las particiones
    """
    for i in range(0, self.numeroParticiones):
        np.random.seed(int(time.time())+i)
        """
        Se crea la particion train a partir de un bucle en el que
        iterando ell tamaño pasado como entrada de los datos, escogemos al azar
        los indices.
        A continuacion con una lista de comprension se crea la lista test
        como los indices que no estan en train pero si en indices
        completando asi las dos listas.
        """
        while len(train) < self.tam:
            train.add(np.random.randint(0, numDatos - 1))
        train = list(train)
        test = [x for x in indices if x not in train]
        #Una vez creadas las divisiones se añade la particion a la lista
        self.particiones.append(Particion(train,test))

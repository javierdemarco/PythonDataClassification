from abc import ABCMeta,abstractmethod
import numpy as np


class Clasificador(object):

    diccionario ={}

    """
    Clase que Define el clasificador, se trata de una clase abstracta que 
    obligara a quienes hereden de ella a definir la funcion de clasificacion y 
    entrenamiento. En el caso de las funciones validacion y error, se definen 
    aqui ya que seran iguales para todos los casos.
    """
    # Clase abstracta
    __metaclass__ = ABCMeta

    """
    Funcion de entranamiento. Se debera implementar en cada uno de los casos 
    especificos.
    datosTrain: matriz numpy con los datos de entrenamiento
    atributosDiscretos: array bool con la indicatriz de los atributos nominales
    diccionario: array de diccionarios de la estructura Datos utilizados para 
    la codificacion de variables discretas
    """
    @abstractmethod
    def entrenamiento(self,datosTrain,atributosDiscretos,diccionario):
        pass

    """
    Funcion de clasificacion. Se debera implementar en cada uno de los casos 
    concretos.
    Devuelve un numpy array con las predicciones
    """
    @abstractmethod
    def clasifica(self,datosTest,atributosDiscretos,diccionario):
        pass

    """
    Funcion de error que obtiene el numero de aciertos y errores para calcular 
    la tasa de fallo.
    """
    def error(self,datos,pred):
        # Aqui se compara la prediccion (pred) con las clases reales y se calcula 
        # el error

        error_porcentaje = 0
        error_lista = list()
        for i in range(len(pred)):
            acierto = 0
            fallo = 0
            #Error con LAPLACE y SIN
            for enum, line in enumerate(datos):
                # print(line)
                # print(pred[0][enum])
                if (int(line[len(line)-1])==pred[i][enum]):
                    acierto +=1
                else:
                    fallo +=1
            print("Aciertos: %d  y Fallos: %d" % (acierto, fallo))
            error_porcentaje = fallo/(fallo+acierto) * 100
            print("Error:", error_porcentaje)
            error_lista.append(error_porcentaje)

        return error_lista

    """
    Funcion de validacion que se encarga de realizar una clasificacion
    utilizando una estrategia de particionado determinada
    """
    # TODO: implementar esta funcion
    def validacion(self,particionado,dataset,clasificador,seed=None):
        
        # Creamos las particiones siguiendo la estrategia llamando a 
        # particionado.creaParticiones
        # - Para validacion cruzada: en el bucle hasta nv entrenamos el 
        # clasificador con la particion de train i
        # y obtenemos el error en la particion de test i
        # - Para validacion simple (hold-out): entrenamos el clasificador con la 
        # particion de train
        # y obtenemos el error en la particion test

        trainList = list()
        testList = list()
        for particion in particionado.particiones:
            trainList = dataset.extraeDatos(particion.indicesTrain)
            testList = dataset.extraeDatos(particion.indicesTest)

        #Fase inicializacion diccionario
        
        #Contamos el numero de clases que tenemos para poder indicar en el numpy array 
        #el numero de columnas
        numClass = set()
        for nCol in trainList:
            numClass.add(int(nCol[len(nCol)-1]))
    
        #Contamos el numero de atributos que tenemos para indica el numero
        #de filas del numpy array
        numAtt = len(trainList[0])-1
        for i in range(numAtt):
            #Usamos set para no tener duplicados
            attArray = set()
            for nFil in trainList:
                attArray.add(int(nFil[i]))
            #Introducimos el numpy array en la posicion key = numero de columna

            if (dataset.nominalAtributos[i]==True):
                array=np.zeros((len(attArray),len(numClass)))    
            else:
                array=np.zeros((2,len(numClass)))    

            self.diccionario[i]=array
            

        # print(self.diccionario)
        clasificador.entrenamiento(trainList,dataset.nominalAtributos,self.diccionario)
        # print(self.diccionario)
        
        pred = list()
        predSin = list()
        predCon = list()
        predSin, predCon = clasificador.clasifica(testList,dataset.nominalAtributos,self.diccionario)

        pred.append(predSin)
        pred.append(predCon)

        return clasificador.error(testList,pred)



        
  
##############################################################################

class ClasificadorNaiveBayes(Clasificador):
    """
    Clasificador Naive Bayes. 
    """


    def __init__(self):
        pass


    # TODO: implementar
    def entrenamiento(self,datostrain,atributosDiscretos,diccionario):
        
        listaNumCol = list()

        col = np.shape(datostrain)[1]-1
        fila = np.shape(datostrain)[0]

        for i in range(col):
            listaNum = list() 
            if (atributosDiscretos[i]==False):
                for j in range(fila):
                    listaNum.append(datostrain[j][i])
                listaNumCol.insert(i,listaNum)
            

        for fila in datostrain:
            k = 0
            for enum, att in enumerate(fila[0:len(fila)-1]):
                clase = int(fila[len(fila)-1])
                att = int(att)-1
                if(atributosDiscretos[enum]==True):
                    diccionario[enum][att][clase]+=1
                else:
                    diccionario[enum][0][clase]=np.mean(listaNumCol[k])
                    diccionario[enum][1][clase]=np.var(listaNumCol[k])
                    k+=1
        
        # print(listaNumCol)

        

    # TODO: implementar
    def clasifica(self,datostest,atributosDiscretos,diccionario):

        predicFinSinLAPACE = list()
        predicFinConLAPACE = list()

        probS = list()
        #Posibles clases de los datos recibidos
        numClass = set()
        for nCol in datostest:
            numClass.add(int(nCol[len(nCol)-1]))

        
        filas = np.shape(datostest)[0]
        col = np.shape(datostest)[1]-1

        #Posibles valores de cada una de las columnas
        valores = list()
        for j in range(col):
            valCol = set()
            for i in range(filas):
                valCol.add(datostest[i][j])
            valores.append(valCol)

        #Diccionario con las probabilidades de cada atributo respecto de la clase P(Xi|Hi)
        diccionarioProb = {}
        for i in range(col):
            probS = np.zeros((len(valores[i]),len(numClass)),dtype=float)
            for atributos in range(len(valores[i])):
                tot=0
                for clases in range(len(numClass)):
                    if(atributosDiscretos[i]==True):
                        tot+=diccionario[i][atributos][clases]
                for clases in range(len(numClass)):
                    if(atributosDiscretos[i]==True):
                        val = diccionario[i][atributos][clases]
                        probS[atributos][clases]=(val/tot)
            diccionarioProb[i]=probS

        #Suma de la cantidad de diferentes clases posibles, agrupados por su indice mostrado en los datostest
        totEachClass = list()
        for att in range(len(valores[0])):
            totEachClass.append(0)
        for i in range(len(numClass)):
            for att in range(len(valores[i])):
                if(atributosDiscretos[i]==True):                
                    totEachClass[i]+=diccionario[0][att][i]

        # print(totEachClass)

        #Calculo de probabilidades clase de cada fila de datos
        predic = np.zeros((filas,len(totEachClass)))
        for enum0, each in enumerate(datostest):
            # print("DATOS:", each)
            for clase in numClass:
                val = 1
                # print("CLASE", clase)
                for enum, att in enumerate(each[:len(each)-1]):
                    att = int(att) - 1
                    # print("Atributo", att)
                    # print("Probabilidad", diccionarioProb[enum][att][clase])
                    val*=diccionarioProb[enum][att][clase]
                # val*=totEachClass[clase]
                val*=(totEachClass[clase]/np.sum(totEachClass))
                predic[enum0][clase]=val
                # print(val)
            # print("Predigo clase:", np.argmax(predic[enum0]))
            predicFinSinLAPACE.append(np.argmax(predic[enum0]))
            
        # print(predic)

        ################################################################
        #                        INICIO DE LA PLACE                    #
        ################################################################


        for clave in self.diccionario.keys():
            nonZero = np.transpose(np.where(self.diccionario[clave]==0))
            if(np.size(nonZero)!=0):
                # print("No hay que aplicar nada")
                self.diccionario[clave]+=1
            # else:
                # print("Hay que aplicar laPlace")
        
        #Suma de la cantidad de diferentes clases posibles, agrupados por su indice mostrado en los datostest
        totEachClass = list()
        for att in range(len(valores[0])):
            totEachClass.append(0)
        for i in range(len(numClass)):
            for att in range(len(valores[i])):
                totEachClass[i]+=diccionario[0][att][i]

        # print(totEachClass)

        #MISMO CODIGO DE PROBABILIDADES, METER EN FUNCION

        #Diccionario con las probabilidades de cada atributo respecto de la clase P(Xi|Hi)
        diccionarioProb = {}
        for i in range(col):
            probS = np.zeros((len(valores[i]),len(numClass)),dtype=float)
            for atributos in range(len(valores[i])):
                tot=0
                for clases in range(len(numClass)):
                    tot+=diccionario[i][atributos][clases]
                for clases in range(len(numClass)):
                    val = diccionario[i][atributos][clases]
                    probS[atributos][clases]=(val/tot)
            diccionarioProb[i]=probS

        #Calculo de probabilidades clase de cada fila de datos
        predic = np.zeros((filas,len(totEachClass)))
        for enum0, each in enumerate(datostest):
            # print("DATOS:", each)
            for clase in numClass:
                val = 1
                # print("CLASE", clase)
                for enum, att in enumerate(each[:len(each)-1]):
                    att = int(att)
                    # print("Atributo", att)
                    # print("Probabilidad", diccionarioProb[enum][att][clase])
                    val*=diccionarioProb[enum][att][clase]
                # val*=totEachClass[clase]
                val*=(totEachClass[clase]/np.sum(totEachClass))
                predic[enum0][clase]=val
                # print(val)
            # print(np.amax(predic[enum0]))
            # print("Predigo clase:", np.argmax(predic[enum0]))
            predicFinConLAPACE.append(np.argmax(predic[enum0]))

            # print(np.amax(predic[enum0]).index)


        # print(predic)

        ################################################################
        #                       FIN DE LA PLACE                        #
        ################################################################

        return predicFinSinLAPACE, predicFinConLAPACE
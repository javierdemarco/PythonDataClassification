# -*- coding: utf-8 -*-
import numpy as np

class Datos(object):
  """
  Clase Datos
  Se encargara de analizar el fichero de entrada y transformarlo en una
  estructura de datos comoda con la que trabajar, numpy
  """
  TiposDeAtributos=('Continuo','Nominal')
  tipoAtributos=list()
  nombreAtributos=[]
  nominalAtributos=[]
  datos=np.array(())
  # Lista de diccionarios. Uno por cada atributo.
  diccionarios=[dict()]

  def __init__(self, nombreFichero):
    i = 0
    matrix = []
    # Open file useing with, which closes file after finish
    with open(nombreFichero,'r') as f:
      # Itarate each line
      for line in f:
      # Ignore the first 2 line
        if (i == 2):
          # Give tipoAtributos the types of atributes in each column
          self.tipoAtributos = line.rstrip('\n\r').split(',')
          # Add it to the corresponding array, whether its nominal or continuo
          for each in self.tipoAtributos:
            if (each == self.TiposDeAtributos[0]):
              self.nombreAtributos.append(each)
            else:
              self.nominalAtributos.append(each)
        # After that is all the same
        if (i > 2):
          # Take the line and insert it in a matrix
          atributos = line.rstrip('\n\r').split(',')
          matrix.append(atributos)
        i += 1
    # Convert the matrix into a numpy array
    self.datos = np.asarray(matrix)
    # When the file ends we can continue processing the data
    # List of comprehension to take the matrix and make a set with the unique
    # values of each column, each row of diccionarios, would be a diccionary
    # for a column in datos
    self.diccionarios = [sorted(set(([row[i] for row in matrix])))
                            for i in range(len(matrix[0]))]
    # Convert diccionarios from set to dicctionary, using the atributo nominal
    # as key, and give it a numerical value.
    i = 0
    for each in self.diccionarios:
      # Only do it if the row is assing to a Nominal atribute, the continuos ones
      # keep their value
      if(self.tipoAtributos[i] == self.TiposDeAtributos[1]):
        self.diccionarios[i] = dict(enumerate(each))
        self.diccionarios[i] = {y: x for x,
                                y in self.diccionarios[i].items()}
      i += 1
    # replace the value in the datos numpy array, for the value given in the
    # earlier created dicctionary
    i = 0
    for aux in self.diccionarios:
      if(self.tipoAtributos[i] == self.TiposDeAtributos[1]):
        for valor, clave in enumerate(aux, 0):
          np.place(self.datos, self.datos == clave, valor)
      i+=1
    # Make the numpy with float type numbers
    self.datos=self.datos.astype(np.float)

  def extraeDatos(self, idx):
    datos_extraidos = list()
    for each in idx:
      datos_extraidos.append(self.datos[each])
    datos_extraidos = np.asarray(datos_extraidos)
    return datos_extraidos


# coding: utf-8

# In[2]:


# -*- coding: utf-8 -*-
"""
Created on Sun Oct 01 20:23:59 2017

@author: profesores faa
"""
import sys
from Datos import Datos
import EstrategiaParticionado as ep
import Clasificador as clf

from sklearn import preprocessing
from sklearn.model_selection import cross_val_score
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score


# dataset=Datos('f:/temp/german.data')
# dataset = Datos('./ConjuntosDatos/balloons.data')
dataset = Datos('./ConjuntosDatos/tic-tac-toe.data')
# dataset = Datos('./ConjuntosDatos/german.data')
# print("-------DATOS--------")
print(dataset.nombreFichero)
#print(dataset.datos)
#Validacion Simple
# print("-------VALIDACION SIMPLE--------")
strategy_simple = ep.ValidacionSimple(0.7, dataset.datos)
# for each in strategy_simple.particiones:
#     print("Test: ", each.indicesTest)
#     print("Train: ", each.indicesTrain)
#     print("---")
#Validacion Cruzada
# print("-------VALIDACION CRUZADA--------")
# strategy_cruzada = ep.ValidacionCruzada(3, dataset.datos)
# for each in strategy_cruzada.particiones:
#     print("Test: " + str(len(each.indicesTest)))
#     print("Train: " + str(len(each.indicesTrain)))
#     print("---")
# #Validacion Bootstrap
# print("-------VALIDACION BOOSTRAP--------")
# strategy_bootstrap = ep.ValidacionBootstrap(3, dataset.datos, 19)
# for each in strategy_bootstrap.particiones:
#     print("Test: " + str(len(each.indicesTest)))
#     print("Train: " + str(len(each.indicesTrain)))
#     print("---")
print("-------NAIVE BAYES--------")
clasificador=clf.ClasificadorNaiveBayes()
errores=clasificador.validacion(strategy_simple,dataset,clasificador)











encAtributos = preprocessing.OneHotEncoder(categorical_features=dataset.nominalAtributos[:-1],sparse=False)
X = encAtributos.fit_transform(dataset.datos[:,:-1])
Y = dataset.datos[:,-1]

gnb = GaussianNB()

# Validacion simple
x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.5)
predicciones = gnb.fit(x_train, y_train).predict(x_test)
print("Porcentaje error con sklearn validacion simple: " + 
str(round((1 - accuracy_score(y_test, predicciones)), 4)))

# Validacion cruzada
# retorna Array of scores of the estimator for each run of the cross validation.
# score = cross_val_score(clf, X, Y, cv=5)
# print("Porcentaje error medio con sklearn validacion cruzada: " + str(round((1-score.mean()), 4)))
# print("Varianza del error con sklearn validacion cruzada: " + str(round((score.std()), 4)))





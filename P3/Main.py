# -*- coding: utf-8 -*-
from Datos import Datos
import EstrategiaParticionado as ep
import Clasificador as cl
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
import plotModel as pM

from plotModel import plotModel
import matplotlib.pyplot as plt

dataset=Datos('ConjuntosDatos_2/wdbc-10.data')


#################################VALIDACION SIMPLE#######################################
strategy_simple = ep.ValidacionSimple(dataset.datos, "Validacion Simple", 1, list(), 0.5)

# #################################VALIDACION CRUZADA#######################################
# strategy_cruzada = ep.ValidacionCruzada(dataset.datos, "Validacion Cruzada", 5, list())

clases = []
for i in dataset.datos:
    clases.append(i[-1])
priori=np.argmax(np.bincount(clases))
print("Priori",priori)

clasificador=cl.ClasificadorAlgoritmoGenetico("ClasificadorAlgoritmoGenetico",
 np.array(()), np.array(()), list(), dict(), 100,80,10,100,20,20,priori)
 #numPoblacion
 #probCruce
 #probMutacion
 #numEpocas
 #probSupervivientes
 #numReglas
 #priori


errores_simple=clasificador.validacion(strategy_simple,dataset,clasificador)
print("\n\tPorcentaje error:", errores_simple,"%")
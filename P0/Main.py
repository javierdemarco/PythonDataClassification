# -*- coding: utf-8 -*-
from Datos import Datos

# dataset=Datos('../ConjuntosDatosP1/tic-tac-toe.data')
dataset=Datos('../ConjuntosDatosP1/german.data')
# dataset=Datos('../ConjuntosDatosP1/balloons.data')
with open('./out.txt', 'w') as f:
  print("----------------------DATOS----------------------\n", file=f)
  for each in dataset.datos:
    print(str(each), file=f)
  print("---------------Tipo De Atributos-----------------\n", file=f)
  for each in dataset.tipoAtributos:
    print(str(each), file=f)
  print("----------------Diccionarios----------------------\n", file=f)
  for each in dataset.diccionarios:
    print(str(each), file=f)
  print("-------------Nombre Atributos----------------------\n", file=f)
  for each in dataset.nombreAtributos:
    print(str(each), file=f)
  print("-------------NOMINAL Atributos---------------------\n", file=f)
  for each in dataset.nominalAtributos:
    print(str(each), file=f)

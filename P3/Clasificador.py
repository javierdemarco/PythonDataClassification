from abc import ABCMeta,abstractmethod
import numpy as np
import math
import operator
import Cromosoma as cr
import copy
import time

class Clasificador(object):
  """
  Clase clasificador que se encargara de realizar la validacion de un metodo de clasificacion
  """

  #Atributos
  nombreEstategia = ""
  datosTrain = np.array(())
  datosTest = np.array(())
  atributosDiscretos = list()
  diccionario = dict()
  normalizado = False

  #Constructor
  def __init__(self, nombreEstategia, datosTrain, datosTest, atributosDiscretos, diccionario):
    self.nombreEstategia = nombreEstategia
    self.datosTrain = datosTrain
    self.datosTest = datosTest
    self.atributosDiscretos = atributosDiscretos
    self.diccionario = diccionario
    return

  # Clase abstracta
  __metaclass__ = ABCMeta

  # Metodos abstractos que se implementan en casa clasificador concreto
  @abstractmethod
  # TODO: esta funcion deben ser implementadas en cada clasificador concreto
  # datosTrain: matriz numpy con los datos de entrenamiento
  # atributosDiscretos: array bool con la indicatriz de los atributos nominales
  # diccionario: array de diccionarios de la estructura Datos utilizados para la codificacion
  # de variables discretas
  def entrenamiento(self,datosTrain,atributosDiscretos,diccionario):
    pass

  @abstractmethod
  # TODO: esta funcion deben ser implementadas en cada clasificador concreto
  # devuelve un numpy array con las predicciones
  def clasifica(self,datosTest,atributosDiscretos,diccionario):
    pass

  # Obtiene el numero de aciertos y errores para calcular la tasa de fallo
  # TODO: implementar
  def error(self,datos,pred):
    # Aqui se compara la prediccion (pred) con las clases reales y se calcula 
    # el error

    error_porcentaje = 0
    # error_lista = list()
    acierto = 0
    fallo = 0
    for enum, line in enumerate(datos):
      # print("Predigo",pred[enum],"es:",line[-1])
      if(line[-1]==pred[enum]):
        acierto +=1
      else:
        fallo +=1
      # print("Aciertos: %d  y Fallos: %d" % (acierto, fallo))
      error_porcentaje = fallo/(fallo+acierto) * 100
      # print("Error:", error_porcentaje)
    
    # error_lista.append(error_porcentaje)
    return error_porcentaje

  # Realiza una clasificacion utilizando una estrategia de particionado determinada
  # TODO: implementar esta funcion
  def validacion(self,particionado,dataset,clasificador,seed=None):
    # Creamos las particiones siguiendo la estrategia llamando a particionado.creaParticiones
    # - Para validacion cruzada: en el bucle hasta nv entrenamos el clasificador con la particion de train i
    # y obtenemos el error en la particion de test i
    # - Para validacion simple (hold-out): entrenamos el clasificador con la particion de train
    # y obtenemos el error en la particion test
    self.datosTrain = dataset.extraeDatos(particionado.listaParticiones[0].indicesTrain)
    self.datosTest = dataset.extraeDatos(particionado.listaParticiones[0].indicesTest)
    self.atributosDiscretos=dataset.nominalAtributos
    clasificador.entrenamiento(self.datosTrain, dataset.nominalAtributos, self.diccionario)
    prediccion = clasificador.clasifica(self.datosTest, self.atributosDiscretos, self.diccionario)

    return self.error(self.datosTest,prediccion)

  def normalizarDatos(self,datos):
    """

    """

    media, desv = self.calcularMediasDesv(self.datosTrain)

    datosTestNormalizados = []
    datosTrainNormalizados = []

    for each in self.datosTrain:
      datoNorm = []
      for enum, dato in enumerate(each):
        if self.atributosDiscretos[enum] == False:
          dato=(dato-media[enum])/desv[enum]
        datoNorm.append(dato)
      datosTrainNormalizados.append(datoNorm)

 
    for each in self.datosTest:
      datoNorm = []
      for enum, dato in enumerate(each):
        if self.atributosDiscretos[enum] == False:
          dato=((dato-media[enum])/desv[enum])
        datoNorm.append(dato)
      datosTestNormalizados.append(datoNorm)

    self.datosTest = datosTestNormalizados
    self.datosTrain = datosTrainNormalizados

  def calcularMediasDesv(self,datostrain):
    """

    """
    media = []
    desviacion = []

    for each in zip(*datostrain):
      media.append(np.mean(each))
      desviacion.append(np.std(each))

    return media, desviacion

##############################################################################

class ClasificadorNaiveBayes(Clasificador):

  def __init__(self, nombreEstategia, datosTrain, datosTest, atributosDiscretos, diccionario):
    super().__init__(nombreEstategia, datosTrain, datosTest, atributosDiscretos, diccionario)
    return

  # TODO: implementar
  def entrenamiento(self,datostrain,atributosDiscretos,diccionario):
    self.diccionario = diccionario
    self.atributosDiscretos = atributosDiscretos
    dataContinuo = list()
    dataNominal = list()
    dataByClass = self.dataSeparateByClass(datostrain)
    #Se dividen los datos entre los que son continuos y nominales. cada fila dentro de la estructura
    #se corresponde con una columna de todos los datos de un mismo atributo
    for i in range(len(dataByClass)):
      eachClass = dataByClass[i]
      #Se crea un array de datos en el que unicamente se incluyen los atributos continuos
      dataContinuo.append([[row[j] for row in eachClass] for j in range(len(eachClass[0]))
                          if atributosDiscretos[j] == False])
      dataNominal.append([[row[j] for row in eachClass] for j in range(len(eachClass[0]))
                          if atributosDiscretos[j] == True])
    #Se calcula la media de los continuos
    dataMean = self.dataCalculateMeanVar(dataContinuo, atributosDiscretos)
    dataProbability = self.dataCalculateProbabilityByClass(dataContinuo, dataMean, dataNominal)
    return np.argmax(dataProbability), np.amax(dataProbability)

  # TODO: implementar
  def clasifica(self,datostest,atributosDiscretos,diccionario):

    pass

  def dataSeparateByClass(self, datos):
    """
    Divide los datos segun la clase a la que pertenezcan
    """
    dataByClass = {}
    #Divide el array de datos segun su clase, que es la ultima columna
    for i in range(len(datos)):
      row = datos[i]
      if (row[-1] not in dataByClass):
        dataByClass[row[-1]] = []
      dataByClass[row[-1]].append(row)
    return dataByClass

  def dataCalculateMeanVar(self, dataContinuo, atributosDiscretos):
    """
    Calcula la media de cada atributo dentro de la clase
    """
    dataMean = list()
    for eachContinuo in dataContinuo:
      #Se itera con cada columna que crea la funcion zip, por cada columna se calcula la media
      #y varianza, añadiendo la tupla a la estructura de dataMean, que sera una lista de tuplas.
      dataMean.append([(np.mean(column), np.var(column)) for column in eachContinuo])
    return dataMean

  def dataCalculateProbabilityByClass(self, dataContinuo, dataMean, dataNominal):
    #Variables Locales
    #Probabilidad a devolver, esta variable al final de la ejecucion tendra el valor maximo de la
    #probabilidad de una clase, habiendo calculado la de todas las filas
    dataProbability = list()
    #Probabilidad de la multiplicacion de las probabilidades de cada una de las filas dividido en clases
    dataProbabilityContinuo = list(list())
    dataProbabilityNominal = list(list())
    #Probabilidad de cada clase
    dataProbabilityClass = list()
    dataClassTotal = 0
    #Se calcula cuantos datos en total hay
    i = 0
    for i in range(len(dataContinuo)):
      dataClassTotal += len(dataContinuo[i][0])
    #Se calcula la probabilidad de cada clase
    i = 0
    dataProbabilityClass = [len(dataContinuo[i][0]) / dataClassTotal for i in range(len(dataContinuo))]
    #Por cada clase
    i = 0
    for eachClass in dataContinuo:
      eachClass = np.asarray(eachClass).T
      dataProbabilityContinuo.append(list())
      #Por cada fila
      dataProbabilityContinuo[i] = [self.dataCalculateProbabilityOfRowContinuo(eachRow,
                                      dataMean[i]) for eachRow in eachClass]
      i+=1
    #Calculo de las probabilidades de los atributos Nominales
    dataProbabilityNominal = self.dataCalculateProbabilityOfClassNominal(dataNominal)
    #Por cada clase
    i = 0
    for i in range(len(dataProbabilityContinuo)):
      dataProbabilityContinuo[i] = np.asarray(dataProbabilityContinuo[i])
      dataProbabilityNominal[i] = np.asarray(dataProbabilityNominal[i])
      dataProbability.append(list())
      #Se multiplican las probabilidades de los dos tipos de atributo de cada linea
      dataProbability[i] = dataProbabilityNominal[i] * dataProbabilityContinuo[i]
      #Cada linea se multiplica por la probabilidad de la clase
      dataProbability[i] *= dataProbabilityClass[i]
      #Se escoge la mayor de las probabilidades de cada clase
      dataProbability[i] = np.amax(dataProbability[i])
    return dataProbability

  def dataCalculateProbabilityOfRowContinuo(self, dataRow, dataMeanRow):
    for i in range(np.size(dataRow[0])):
      dataProbabilityAtribute = self.dataCalculateProbabilityOfAtributeContinuous(dataRow[0], dataMeanRow[0])
      for cont in range(len(dataRow) - 1):
        dataProbabilityAtribute *= self.dataCalculateProbabilityOfAtributeContinuous(dataRow[cont], dataMeanRow[cont])
    return dataProbabilityAtribute

  def dataCalculateProbabilityOfClassNominal(self, dataNominal):
    dataProbabilityNominal = list(list())
    dataRowDiccionary = list({})
    #Bucle para encontrar el numero de ocurrencias de cada uno de los atributos
    # fila = [row for row in self.diccionario if self.atributosDiscretos[] == True]
    dataRowDiccionary = self.diccionario
    i = 0
    for eachRow in dataRowDiccionary:
      if self.atributosDiscretos[i] == False:
        dataRowDiccionary.remove(eachRow)
      i+=1
    i = 0
    for eachClass in dataNominal:
      dataRowDiccionary.append(list())
      for eachRow in eachClass:
        unique, counts = np.unique(eachRow, return_counts=True)
        dataRowDiccionary[i].append(dict(zip(unique, counts)))
      i+=1
    
    #Calculo de la probabilidad de cada linea de cada Clase
    i = 0
    for eachClass in dataNominal:
      dataProbabilityNominal.append(list())
      j = 0
      for eachRow in eachClass:
        dataProbabilityNominal[i].append(self.dataCalculateProbabilityOfRowNominal(eachRow,
                                          dataRowDiccionary[i][j], 
                                          dataRowDiccionary[i][len(dataRowDiccionary[i]) - 1], i))      
        j+=1                                    
      i+=1
    return dataProbabilityNominal

  def dataCalculateProbabilityOfRowNominal(self, dataRow, dataRowDiccionary, dictClass, dataClass):
    dataProbabilityAtribute = {}
    #Calculo de la probabilidad de cada fila
    for cont in range(len(dataRow)):
      if dataRow[cont] not in dataProbabilityAtribute:
        dataProbabilityAtribute[dataRow[cont]] = []
      dataProbabilityAtribute[dataRow[cont]] = self.dataCalculateProbabilityOfAtributeNominal(dataRow[cont],
                      dataRowDiccionary, dictClass, dataClass)
      
    return dataProbabilityAtribute

  def dataCalculateProbabilityOfAtributeContinuous(self, dataAtribute, dataMeanAtribute):
    #Calcular el exponente de la formula de GaussNB
    exp = math.exp(-(math.pow(dataAtribute-dataMeanAtribute[0],2)/(2*math.pow(dataMeanAtribute[1],2))))
    #Calcular la probabilidad de la observacion con respecto a la clase, teniendo el exponente
    dataProbability = (1 / (math.sqrt(2*math.pi) * dataMeanAtribute[1])) * exp
    return dataProbability

  def dataCalculateProbabilityOfAtributeNominal(self, dataAtribute, dataRowDiccionary, dataClassTotal,
                                                      dataClass):
    #La probabilidad es el numero de ocurrencias en los datos, divido por el numero total de datos
    # de la clase
    return dataRowDiccionary.get(dataAtribute) / dataClassTotal.get(dataClass)

class ClasificadorVecinosProximos(Clasificador):

  k = 0

  def __init__(self, nombreEstategia, datosTrain, datosTest, atributosDiscretos, diccionario, normalizado, k):
    super().__init__("ClasificadorVecinosProximos", datosTrain, datosTest, atributosDiscretos, diccionario)
    self.normalizado = normalizado
    self.k = k
    return

  def validacion(self, particionado, dataset, clasificador, seed=None):

    self.datosTrain = dataset.extraeDatos(particionado.listaParticiones[0].indicesTrain)
    self.datosTest = dataset.extraeDatos(particionado.listaParticiones[0].indicesTest)
    
    self.atributosDiscretos = dataset.nominalAtributos

    if self.normalizado == True:
      self.normalizarDatos(dataset)
   
    predicciones=self.clasifica(self.datosTest,self.atributosDiscretos,dataset.diccionarios)

    error = self.error(self.datosTest,predicciones)    
    return error
  
  def clasifica(self, datostest, atributosDiscretos, diccionario):  
    predicciones = []
    if(self.k>len(self.datosTrain)):
      print("No hay tantos vecinos, (",self.k,")")
    else:
      for i in range(len(self.datosTest)):
        vecinos = self.vecinosProximos(self.datosTrain,self.datosTest[i],self.k)
        prediccion = self.getPrediccion(vecinos)
        predicciones.append(prediccion)
    return np.array(predicciones)
  
  
  def vecinosProximos(self, datosTrain, datoTest, k):
    """
    Calcula los vecinos proximos a datoTest
    """
    distancias = []
    length = len(datoTest)-1
    for i in range(len(datosTrain)):
      dist = self.distanciaElementos(datoTest,datosTrain[i],length)
      distancias.append((datosTrain[i],dist))
    
    #Sort de mayor a menor
    distancias = sorted(distancias, key=operator.itemgetter(1))

    vecinos = []
    for i in range(k):
      vecinos.append(distancias[i][0])
    
    return vecinos

  def distanciaElementos(self, el1, el2, length):

    dist = 0
    for i in range(length):
      dist += math.pow(((el1[i] - el2[i])/2),2)
    
    return math.sqrt(dist)


  def getPrediccion(self, vecinos):
    
    clases = {}
    for x in range(len(vecinos)):
      response = vecinos[x][-1]
      if response in clases:
        clases[response] += 1
      else:
        clases[response] = 1
    prediccion = sorted(clases.items(), key=operator.itemgetter(1), reverse=True)
    return prediccion[0][0]

class ClasificadorRegresionLogistica(Clasificador):

  def __init__(self, nombreEstategia, datosTrain, datosTest, atributosDiscretos, diccionario, constAprendizaje,nEpocas,w, normalizado):
    super().__init__("ClasificadorVecinosProximos", datosTrain, datosTest, atributosDiscretos, diccionario)
    self.constAprendizaje = constAprendizaje
    self.nEpocas = nEpocas
    self.w = w
    self.normalizado = normalizado

  def sigmoidal(self,w,p):
    if len(w)>len(p):
      p=np.append([1],p[:])
    elif len(p)>len(w):
      w=np.append([1],w[:])

    try:
        aux=1.0/(1+math.exp(-np.dot(w,p)))
    except OverflowError:
        aux= 0.0
    return aux

  def entrenamiento(self, datostrain, atributosDiscretos, diccionario):

    if self.normalizado == True:
      self.normalizarDatos(datostrain)
    for i in range(len(self.datosTrain[0])):
      self.w.append(np.random.uniform(-0.5,0.5))

    for i in range(self.nEpocas):
      for fila in datostrain:
        aux = np.append([1],fila[:-1])
        sigma=self.sigmoidal(self.w,fila)
        self.w = self.w - (self.constAprendizaje*(sigma-fila[-1]))*fila

  def clasifica(self, datostest, atributosDiscretos, diccionario):
    
    prediccion = []
    for fila in datostest:
      if self.sigmoidal(self.w,fila) >= 0.5 :
        prediccion.append(1)
      else:
        prediccion.append(0)

    return np.array(prediccion)

class ClasificadorAlgoritmoGenetico(Clasificador):

  numIntervalos = 0
  poblacion = []
  newPoblacion = []
  numAtt = 0
  probCruce = 0
  probMutacion = 0
  nEpocas = 0
  localFitness = 0
  probSupervivientes = 0
  fitnessChoice = []
  numReglas = 0
  priori = 0

  def __init__(self, nombreEstategia, datosTrain, datosTest, atributosDiscretos, diccionario,
   numPobl, probCruce, probMutacion, nEpocas, probSupervivientes,numReglas,priori):
    super().__init__("ClasificadorAlgoritmoGenetico", datosTrain, datosTest, atributosDiscretos, diccionario)
    self.numPobl = numPobl
    self.probCruce = probCruce
    self.probMutacion = probMutacion
    self.nEpocas = nEpocas
    self.probSupervivientes = probSupervivientes
    self.numReglas = numReglas
    self.priori = priori

  def tratamientoDatasetAG(self, dataset):

    self.numAtt = dataset.datos.shape[1]
    K = int(1 + 3.322*math.log(dataset.datos.shape[0],10))
    self.numIntervalos = K
    
    intervalos=[]
    minimos=[]
    for i in zip(*dataset.datos):
      intervalos.append((np.max(i) - np.min(i))/K)
      minimos.append(np.min(i))
      
    for i in range(dataset.datos.shape[0]):
      for j in range(dataset.datos.shape[1]-1):
        dataset.datos[i][j]=(int((dataset.datos[i][j]-minimos[j])/intervalos[j])+1)
  
    return dataset

  def crearPoblacion(self):

    poblacion = []
    for i in range(self.numPobl):
      poblacion.append(self.crearCromosoma())
    return poblacion    
  
  def crearCromosoma(self):
    
    numReglas = np.random.randint(1,self.numReglas)

    cromosoma = []
    for i in range(numReglas):
      array0=np.random.binomial(1,0.1,self.numAtt-1)
      array1=np.random.randint(0,11,self.numAtt-1)
      arr=list(array0*array1)
      arr.append(np.random.randint(1))
      cromosoma.append(arr)
    
    individuo = cr.Cromosoma(cromosoma,0)

    return individuo
  
  def crearCromosoma_binario(self):
    
    numReglas = np.random.randint(1,self.numReglas)

    # cromosoma = []
    # np.random.binomial
    # for i in range(numReglas):
    #   for i in range(len()))
    #   np.random.binomial(1,0.5,(self.numIntervalos))
    #   cromosoma.append(arr)
    
    individuo = cr.Cromosoma(cromosoma,0)

    return individuo

  def validacion(self, particionado, dataset, clasificador, seed=None):

    self.atributosDiscretos=dataset.nominalAtributos

    clasificador.tratamientoDatasetAG(dataset)

    self.datosTrain = dataset.extraeDatos(particionado.listaParticiones[0].indicesTrain)
    self.datosTest = dataset.extraeDatos(particionado.listaParticiones[0].indicesTest)

    poblacion = self.crearPoblacion()

    diccionario = {}
    self.entrenamiento(self.datosTrain,dataset.nominalAtributos,diccionario,poblacion)
    pred = self.clasifica(self.datosTest,dataset.nominalAtributos,diccionario,poblacion)
    return self.error(self.datosTest,pred)
      
  def entrenamiento(self,datosTrain,atributosDiscretos,diccionario,poblacion):
    # start_time = time.time()

    for i in range(self.nEpocas):
      print("------- Epoca ",i,"------------")
      poblacion = self.seleccionProgenitores(datosTrain,poblacion)

      #MaximoMedia
      self.Apartado4(poblacion)

      hijos = self.cruce(poblacion)
      mutados = self.mutacion(hijos)
      poblacion = self.elitismo(poblacion,mutados,datosTrain)

      # print("Epoca",i,"done")
      # fin_time = time.time()
      # print((fin_time-start_time),"segundos")

  def fitness(self,datosTrain, poblacion):
    
    for indivi in poblacion:
      indivi.fitness = 0
      for ejemplo in datosTrain:
        prediccion = []
        #Cada regla del individuo
        for reglaIndi in indivi.reglas:        
          concurrencia = 0
          #Comparacion de reglas
          for i in range(len(ejemplo)-1):
            if reglaIndi[i] == 0 or reglaIndi[i] == ejemplo[i]:
              concurrencia+=1

          if concurrencia == (len(ejemplo)-1):
            prediccion.append(reglaIndi[-1])
          else: 
            prediccion.append(self.priori)

        #Mismo numero de 0 que de 1
        unos=np.count_nonzero(prediccion)
        ceros=np.absolute(len(prediccion)-unos)
        if unos == ceros:
          #comparamos con priori
          if ejemplo[-1] == self.priori:
            indivi.fitness+=1
        #El que mas predigamos
        elif ejemplo[-1] == np.argmax(np.bincount(prediccion)):
          indivi.fitness+=1

      indivi.fitness = indivi.fitness/datosTrain.shape[0]

  def cruce(self,poblacion):
    """
    """

    hijos=[]    

    for i in range(0,len(poblacion),2):
      cromosoma0 = poblacion[i]
      if len(poblacion)%2 == 0:
        cromosoma1 = poblacion[i+1]
      else:
        cromosoma1 = poblacion[i]

      if np.random.randint(100) < self.probCruce :

        corteCromo0 = np.random.randint(len(cromosoma0.reglas))
        corteCromo1 = np.random.randint(len(cromosoma1.reglas))

        reglasCorteCromo0 = cromosoma0.reglas[corteCromo0:]
        reglasCorteCromo1 = cromosoma1.reglas[corteCromo1:]

        del cromosoma0.reglas[corteCromo0:]
        del cromosoma1.reglas[corteCromo1:]

        cromosoma0.reglas+=reglasCorteCromo0
        cromosoma1.reglas+=reglasCorteCromo1

      hijos.append(copy.deepcopy(cromosoma0))
      hijos.append(copy.deepcopy(cromosoma1))

    return hijos

  def mutacion(self,poblacion):

    for indivi in poblacion:
      for regla in indivi.reglas:
        for att in range(len(regla)):
          if np.random.randint(100) < self.probMutacion:
            valor = np.random.randint(self.numIntervalos)
            regla[att]=valor
    return poblacion

  def seleccionProgenitores(self, datosTrain, poblacion):

    self.fitness(datosTrain, poblacion)

    #Fitness por generacion
    # suma = sum(each.fitness for each in self.poblacion)
    fitnessChoiceProb = self.ajusteProb(poblacion,1)

    print("\t\t\t\tProb",sum(fitnessChoiceProb))
    fitnessChoice = np.random.choice(len(poblacion),len(poblacion),p=fitnessChoiceProb)
    nuevaPoblacion = []
    for i in fitnessChoice:
      nuevaPoblacion.append(poblacion[i])

    return nuevaPoblacion

  def elitismo(self,poblacion,mutados,datosTrain):

    self.fitness(datosTrain,mutados)
    self.ajusteProb(mutados,0)
    poblacion.sort(key=operator.attrgetter('fitnessProb'), reverse=True)
    mutados.sort(key=operator.attrgetter('fitnessProb'), reverse=False)

    numSupervivientes = self.probSupervivientes*len(poblacion)/100
    for i in range(int(numSupervivientes)):
      mutados[i]=poblacion[i]

    return mutados

  def clasifica(self,datosTest,atributosDiscretos,diccionario,poblacion):
    
    poblacion.sort(key=operator.attrgetter('fitness'), reverse=True)

    prediccion = []
    for each in datosTest:
      for cromosoma in poblacion:
        for regla in cromosoma.reglas:
          concurrencia = 0
          for i in range(len(regla)-1):
            if each[i] == regla [i] or regla == 0:
              concurrencia+=1
          if concurrencia == len(regla)-1:
            prediccion.append(regla[-1])
          else:
            prediccion.append(np.random.randint(0,1))        

    return prediccion

  def Apartado4(self,poblacion):
    objCromo = max(poblacion,key=operator.attrgetter('fitness'))
    print("Maximo fit:", objCromo.fitness)
    suma = 0
    for j in poblacion:
      suma+=j.fitness
    print("Media fit:", suma/len(poblacion))

  def ajusteProb(self,poblacion,flag):

    fitnessChoiceProb = []
    suma = 0
    for each in poblacion:
      suma+=each.fitness
    for each in poblacion:
      if suma != 0:
        each.fitnessProb=each.fitness/suma
      else:
        each.fitnessProb=0
      if flag == 1:
        fitnessChoiceProb.append(each.fitnessProb)
    
    return fitnessChoiceProb